HitcentsParkPlaza::Application.routes.draw do


  mount Ckeditor::Engine => '/ckeditor'
  mount HitcentsAuth::Engine, :at => '/hitcents', :as => 'hitcents'


  devise_for :admin_users, ActiveAdmin::Devise.config
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  root :to => 'pages#home'

  get "eat/:restaurant/menus" => 'menus#index'
  get "eat/:restaurant/menu/:id" => 'menus#show'

  get "eat/" => 'restaurants#index', :as => 'eat'
  get "eat/:restaurant" => 'restaurants#show'

  get "eat/:restaurant/careers" => 'job_openings#show'
  get "work/careers" => 'job_openings#index'
  get "job_openings/:id/job_applications" => 'job_applications#new'


  get "eat/:restaurant/photos" => 'gallery_photos#show'
  get "/photos" => 'gallery_photos#index'

  get "work" => 'pages#work', :as => 'work'

  get "play" => 'special_events#index', :as => 'play'
  get "banquet" => 'banquet_menu_items#index', :as => 'banquet'

  get "about" => 'pages#about', :as => 'about'
  get "contact" => 'pages#contact', :as => 'contact'
  get "rewards" => 'pages#rewards', :as => 'rewards'

  resource :email_submissions, only: [:create]

  resources :restaurants do
    resources :job_openings
    resources :menus
    resources :gallery_photos
  end

  resources :job_openings do
    resource :job_applications, only: [:new, :create]
  end

  resources :gallery_photos
  resources :special_events
  resources :gift_cards



  ActiveAdmin.routes(self)

end
