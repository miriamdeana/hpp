set :application, "hitcents-park-plaza"
server "webserver3.hitcents.com", :app, :web, :db, :primary => true

set :stages, %w(production beta)
set :default_stage, "beta"

require 'capistrano/ext/multistage'

require 'capistrano-hitcents'
