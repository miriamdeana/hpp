class JobApplicationsController < ApplicationController
  def new
    @job_application = JobApplication.new
  end

  def create
    @job_opening = JobOpening.find(params[:job_opening_id])
    @job_application = @job_opening.job_applications.create(job_application_params)

    if @job_application.save
      format.html { redirect_to job_openings_path, notice: "Thanks for your application."
      format.json { head :no_content}
    else
      format.html { render action: 'new'}
      format.json { render json: @job_application.errors, status: 406 }
    end
  end

  private

    def job_application_params
      params.require(:job_application).permit(:name, :last_name, :email, :phone,
        :address_1, :address_2, :city, :state, :zip)
    end
end
