class EmailSubmissionsController < ApplicationController
  def create
    @email_submission = EmailSubmission.new(email_submission_params)
    if @email_submission.save
      render json: @email_submission, status: 200
    else
      render json: @email_submission, status: 406
    end
  end

  private

    def email_submission_params
      params.require(:email_submission).permit(:email, :first_name, :last_name)
    end
end
