class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :determine_restaurant, :email_submission

  def determine_restaurant
    if params[:restaurant]
      @@restaurant = Restaurant.find_by_name(params[:restaurant])
    end
  end

  def email_submission
    @email_submission = EmailSubmission.new
  end
end
