class PagesController < ApplicationController
  def home

    # render :coming_soon
  end

  def work
  end

  def play
  end

  def about
  end

  def contact
  end

  def rewards
  end
end
