class SpecialEventsController < ApplicationController
  def index
    @special_events = SpecialEvent.all
  end

  def show
    @special_events = SpecialEvent.find(params[:id])
  end
end
