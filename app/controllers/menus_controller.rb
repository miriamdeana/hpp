class MenusController < ApplicationController
  def index
    @menus = @@restaurant.menus
  end

  def show
    @menu = Menu.find_by_id(params[:id])
    @menu_items = @menu.menu_items
  end
end
