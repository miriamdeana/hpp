$(window).bind('page:change', function() {

  $("form#new_email_submission")
    .on('ajax:success', function(event, xhr, status) {
      email_submission_message('success', 'Thanks for signing up!');
      $("#email_submission_email").val("");
    })
    .on('ajax:error', function(event, xhr, status) {
      email_submission_message('error', 'There was a problem. Please try again.');
    })
    ;

  function email_submission_message(type, message) {
    $msg = $(".email-submission-form-msg");
    $msg.removeClass("alert alert-success alert-danger");

    if (type == 'success') {
      $msg.html(message).addClass('alert alert-success');
    }
    else if (type == 'error') {
      $msg.html(message).addClass('alert alert-danger');
    }
  }

});
