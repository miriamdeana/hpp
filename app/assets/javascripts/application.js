// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require turbolinks
//= require ckeditor/init
//= require_tree .


var ready = function(){
  //= Set height from browser window
  var pageHeight = $(window).height();
  $('.bucket, .navbar-fixed, .block').css('height', pageHeight);

  //Underline active nav link
    $(".sidebar-nav li a").each(function(index) {
        if(this.href.trim() == window.location)
            $(this).parent().addClass("selected");
    });


  //= Overlay hover function on home page
  $(".block").hover(function(){
    $(this).stop(true).animate({
      right: '-= 280px',
      width: '0%'
    }, 750);
  }, function(){
    $(this).stop(true).animate({
      right: '+= 280px',
      width: '28.5%'
    }, 500);
  }
  );
};

$(document).ready(ready);
$(document).on('page:load', ready);