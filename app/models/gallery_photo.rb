class GalleryPhoto < ActiveRecord::Base
  belongs_to :restaurant

  has_attached_file :photo,
    :styles => { :photo => "386x224", :thumb => "150x100>" }
end
