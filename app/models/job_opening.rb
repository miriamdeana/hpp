class JobOpening < ActiveRecord::Base

  belongs_to :restaurant
  has_many :job_applications
  before_save do
    self.post_date = Date.today
  end

end
