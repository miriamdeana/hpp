class SpecialEvent < ActiveRecord::Base
  belongs_to :restaurant

  has_attached_file :image
end
