class Restaurant < ActiveRecord::Base
  has_attached_file :photo,
    :styles => { :photo => "386x224", :thumb => "150x100>" },
    :default_url => "/images/:style/missing.png"

  has_many :menus
  has_many :special_events
  has_many :job_openings
  has_many :gallery_photos
end
