class Menu < ActiveRecord::Base
  belongs_to :restaurant
  has_many :categories

  accepts_nested_attributes_for :categories
end
