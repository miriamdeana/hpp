class EmailSubmission < ActiveRecord::Base
  validates :email, presence: true
end
