class JobApplication < ActiveRecord::Base
  belongs_to :job_opening

  validates_presence_of :email, :name, :last_name, :job_opening_id
end
