ActiveAdmin.register MenuItem do

  filter :menu
  filter :restaurant
  filter :name
  filter :description

  controller do
    def permitted_params
      params.permit menu_item: [:name, :description, :size, :price,
        :image, :category_id, :restaurant_id]
    end
  end

  index do |item|
    column :menu do |item|
      link_to item.category.menu.name, admin_menu_path(item.category.menu)
    end
    column :category
    column :name
    column :description
    column :size
    column :price
    column :image do |item|
      image_tag(item.image.url)
    end
    actions
  end

  form do |f|
    f.inputs "Menu Item" do
      f.input :category_id, :label => 'Category', :as => :select, :collection => Category.all.map{|c| ["#{c.name} - #{c.menu.name} - #{c.menu.restaurant.name}", c.id]}
      f.input :name
      f.input :description, :as => :ckeditor
      f.input :size
      f.input :price
      f.input :image
    end

    f.actions
  end

  show do |item|
    attributes_table do
      row :menu do |item|
        item.category.menu
      end
      row :category do |item|
        item.category
      end
      row :name
      row :description
      row :price
      row :image do |item|
        image_tag(item.image.url)
      end
    end
  end
end
