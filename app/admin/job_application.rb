ActiveAdmin.register JobApplication do
  actions :all, except: [:destroy]

  controller do
    def permitted_params
      params.permit job_application: [:name, :last_name, :email, :phone, :address_1,
      :address_2, :city, :state, :zip, :job_opening_id]
    end
  end

  index do |app|
    column :job_opening do |app|
      link_to app.job_opening.title, admin_job_opening_path(app.job_opening)
    end
    column :name
    column :last_name
    column :email
    column :phone

    actions
  end

  show do |app|
    attributes_table do
      row :job_opening do |app|
        app.job_opening
      end
      row :name
      row :last_name
      row :email
      row :phone
      row :address_1
      row :address_2
      row :city
      row :state
      row :zip
    end
  end

  form do |f|
    f.inputs "Job Application" do
      f.input :job_opening
      f.input :name, :label => "First name"
      f.input :last_name
      f.input :email
      f.input :address_1
      f.input :address_2
      f.input :city
      f.input :state
      f.input :zip
    end
    f.actions
  end

  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

end
