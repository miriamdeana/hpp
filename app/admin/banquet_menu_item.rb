ActiveAdmin.register BanquetMenuItem do

  permit_params :name, :description, :price_per_person, :image

  form do |f|
    f.inputs do
      f.input :name
      f.input :description, :as => :ckeditor
      f.input :price_per_person
      f.input :image
    end
    f.actions
  end

  index do |item|
    column :name
    column :description
    column :price_per_person
    column :image do |item|
      image_tag(item.image.url)
    end
  end

  show do |item|
    attributes_table do
      row :name
      row :description
      row :price_per_person
      row :image do |item|
        image_tag(item.image.url)
      end
    end
  end
end
