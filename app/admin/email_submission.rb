ActiveAdmin.register EmailSubmission do


  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #

  controller do
    def permitted_params
      params.permit email_submission: [:first_name, :last_name, :email]
    end
  end

  form do |f|
    f.inputs "email_submission" do

      f.input :first_name
      f.input :last_name
      f.input :email

    end
    f.actions
  end
end
