ActiveAdmin.register JobOpening do


  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #

  controller do
    def permitted_params
      params.permit job_opening: [:title, :description, :post_date, :closing_date, :restaurant_id]
    end
  end

  form do |f|
    f.inputs "job_opening" do
      f.input :restaurant, :as => :select, :collection => Restaurant.all
      f.input :title
      f.input :description, :as => :ckeditor
      f.input :closing_date
    end
    f.actions
  end
end
