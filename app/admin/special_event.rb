ActiveAdmin.register SpecialEvent do

  permit_params :title, :start_date_time, :end_date_time, :location, :description, :restaurant_id, :image

  index do |e|
    column :restaurant
    column :title
    column :location
    column :description
    column :start_date_time
    column :end_date_time
    e.actions
  end

  form do |f|
    f.inputs "Special Event" do
      f.input :restaurant
      f.input :image
      f.input :title
      f.input :location
      f.input :description
      f.input :start_date_time
      f.input :end_date_time
    end
    f.actions
  end

  show do |event|
    attributes_table do
      row :restaurant
      row :title
      row :image  do |event|
        image_tag(event.image.url) unless event.image.blank?
      end
      row :location
      row :description
      row :start_date_time
      row :end_date_time
    end
  end
end
