ActiveAdmin.register GalleryPhoto do

  controller do
    def permitted_params
      params.permit gallery_photo: [:photo_file_name, :photo_file_size,
        :caption, :restaurant_id]
    end
  end

  form do |f|
    f.inputs "gallery_photo" do
      f.input :photo
      f.input :caption
      f.input :restaurant
    end

    f.actions
  end

end
