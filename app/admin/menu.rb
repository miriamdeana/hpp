ActiveAdmin.register Menu do
  permit_params :name, :restaurant_id, :category_id, :id, :categories_attributes =>
  [:id, :name]

  form do |f|
    f.inputs "Menu" do
      f.input :restaurant
      f.input :name

      f.inputs "Categories" do
        f.has_many :categories, :header => 'Categories' do |cat|
          cat.input :name
        end

      end #cat
      f.actions
    end #menu
  end #form

end
