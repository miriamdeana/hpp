ActiveAdmin.register Restaurant do
  actions :all, except: [:destroy]
  config.clear_sidebar_sections!

  permit_params :name, :description, :photo, :hours

  controller do
    def permitted_params
      params.permit restaurant: [:name, :description, :photo, :hours]
    end
  end

  index do |r|
    column :name
    column :description
    column :hours
    column :photo do |r|
      image_tag(r.photo.url(:thumb))
    end
    r.actions
  end

  form do |f|
    f.inputs "restaurant" do
      f.input :name
      f.input :description
      f.input :photo
      f.input :hours
    end

    f.actions
  end
end
