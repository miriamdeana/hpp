# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
if Rails.env.development? || Rails.env.test?

  Restaurant.create!(name: "Mariahs",
    hours: "M-F 11 - 9",
    description: "Mariahs description for the Mariahs restaurant concept.",
    photo: File.open('app/assets/images/mariahs.jpg'))

  Restaurant.create!(name: "Brick-and-Basil",
    hours: "M-F 11 - 10",
    description: "Brick & Basil description for the Brick & Basil restaurant concept.",
    photo: File.open('app/assets/images/brick.jpeg'))
  Restaurant.create!(name: "Pagoda",
    hours: "M-F 5 - 10",
    description: "Pagoda description for the Pagoda restaurant concept.",
    photo: File.open('app/assets/images/pagoda.jpg'))

  Restaurant.create!(name: "Tres-Molinos",
    hours: "M-F 11 - 7",
    description: "Tres Molinos description for the Tres Molinos restaurant concept.",
    photo: File.open('app/assets/images/molinos.jpg'))

  Restaurant.create!(name: "643",
    hours: "M-F 11 - 12",
    description: "643 description for the 643 restaurant concept.",
    photo: File.open('app/assets/images/643.jpeg'))

end


1...5 do |i|
  se = SpecialEvent.create ({
    :title => "Title #{i}",
    :location => "Location #{i}",
    :description => "Description #{i}",
    :restaurant_id => "#{i}",
    :start_date_time => Time.now,
    :end_date_time => Time.now
  })

  se.image = File.open('app/assets/images/event1.png')

  se.save!
end

AdminUser.create!(:email => "admin@example.com",
  :password => 'password',
  :password_confirmation => 'password')