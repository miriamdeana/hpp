class CreateGiftCards < ActiveRecord::Migration
  def change
    create_table :gift_cards do |t|
      t.string :purchaser_first_name
      t.string :purchaser_last_name
      t.string :recipient
      t.string :ship_address
      t.string :bill_address
      t.string :from_email
      t.string :to_email
      t.integer :tax
      t.integer :total

      t.timestamps
    end
  end
end
