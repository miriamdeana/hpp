class AddTitleToSpecialEvents < ActiveRecord::Migration
  def change
    add_column :special_events, :title, :string
  end
end
