class AddCategoryToMenuItems < ActiveRecord::Migration
  def change
    add_reference :menu_items, :category, index: true
  end
end
