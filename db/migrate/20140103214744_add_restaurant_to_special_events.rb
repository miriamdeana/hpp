class AddRestaurantToSpecialEvents < ActiveRecord::Migration
  def change
    add_column :special_events, :restaurant_id, :integer
    add_index :special_events, :restaurant_id
  end
end
