class CreateSpecialEvents < ActiveRecord::Migration
  def change
    create_table :special_events do |t|
      t.date :date
      t.time :start_time
      t.time :end_time
      t.string :location
      t.text :description

      t.timestamps
    end
  end
end
