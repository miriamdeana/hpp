class RemoveTimesFromSpecialEvents < ActiveRecord::Migration
  def change
    remove_column :special_events, :start_time, :time
    remove_column :special_events, :end_time, :time
  end
end
