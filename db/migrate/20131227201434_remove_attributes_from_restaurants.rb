class RemoveAttributesFromRestaurants < ActiveRecord::Migration
  def change
    remove_column :restaurants, :slug, :string
    remove_column :restaurants, :hostname, :string
  end
end
