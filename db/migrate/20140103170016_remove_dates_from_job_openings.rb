class RemoveDatesFromJobOpenings < ActiveRecord::Migration
  def change
    remove_column :job_openings, :post_date, :string
    remove_column :job_openings, :closing_date, :string
  end
end
