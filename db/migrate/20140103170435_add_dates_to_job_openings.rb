class AddDatesToJobOpenings < ActiveRecord::Migration
  def change
    add_column :job_openings, :post_date, :date
    add_column :job_openings, :closing_date, :date
  end
end
