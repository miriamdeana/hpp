class CreateBanquetMenuItems < ActiveRecord::Migration
  def change
    create_table :banquet_menu_items do |t|
      t.string :name
      t.string :description
      t.string :price_per_person

      t.timestamps
    end
  end
end
