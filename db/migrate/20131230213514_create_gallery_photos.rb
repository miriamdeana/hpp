class CreateGalleryPhotos < ActiveRecord::Migration
  def change
    create_table :gallery_photos do |t|
      t.has_attached_file :photo
      t.string :caption

      t.timestamps
    end
  end
end
