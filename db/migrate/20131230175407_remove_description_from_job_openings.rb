class RemoveDescriptionFromJobOpenings < ActiveRecord::Migration
  def change
    remove_column :job_openings, :description, :string
  end
end
