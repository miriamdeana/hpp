class RemoveDateFromSpecialEvents < ActiveRecord::Migration
  def change
    remove_column :special_events, :date, :date
  end
end
