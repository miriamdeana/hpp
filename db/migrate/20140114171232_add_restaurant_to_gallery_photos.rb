class AddRestaurantToGalleryPhotos < ActiveRecord::Migration
  def change
    add_reference :gallery_photos, :restaurant, index: true
  end
end
