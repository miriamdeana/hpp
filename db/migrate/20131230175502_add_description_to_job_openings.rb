class AddDescriptionToJobOpenings < ActiveRecord::Migration
  def change
    add_column :job_openings, :description, :text
  end
end
