class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.string :name
      t.boolean :active
      t.references :restaurant
      t.timestamps
    end
  end
end
