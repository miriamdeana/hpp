class AddTimesToSpecialEvents < ActiveRecord::Migration
  def change
    add_column :special_events, :start_date_time, :datetime
    add_column :special_events, :end_date_time, :datetime
  end
end
