class AddAttributesToRestaurant < ActiveRecord::Migration
  def change
    change_table :restaurants do |t|
     t.has_attached_file :photo
     t.text :hours
     t.text :description
    end
  end
end
