class ChangeDecimalInMenuItems < ActiveRecord::Migration
  def change
    reversible do |dir|
      change_table :menu_items do |t|
        dir.up   { t.change :price, :string}
        dir.down { t.change :price, :decimal}
      end
    end
  end
end
