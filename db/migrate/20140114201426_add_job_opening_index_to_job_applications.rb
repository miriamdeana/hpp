class AddJobOpeningIndexToJobApplications < ActiveRecord::Migration
  def change
    add_reference :job_applications, :job_opening, index: true
  end
end
