class CreateMenuItems < ActiveRecord::Migration
  def change
    create_table :menu_items do |t|
      t.string :name
      t.text :description
      t.string :size
      t.decimal :price
      t.string :category
      t.attachment :image
      t.references :menu

      t.timestamps
    end
  end
end
