class CreateJobOpenings < ActiveRecord::Migration
  def change
    create_table :job_openings do |t|
      t.string :title
      t.string :description
      t.datetime :post_date
      t.datetime :closing_date
      t.references :restaurant
      t.timestamps
    end
  end
end
