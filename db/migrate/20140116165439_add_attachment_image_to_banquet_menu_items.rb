class AddAttachmentImageToBanquetMenuItems < ActiveRecord::Migration
  def self.up
    change_table :banquet_menu_items do |t|
      t.attachment :image
    end
  end

  def self.down
    drop_attached_file :banquet_menu_items, :image
  end
end
