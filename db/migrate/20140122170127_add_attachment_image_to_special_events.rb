class AddAttachmentImageToSpecialEvents < ActiveRecord::Migration
  def self.up
    change_table :special_events do |t|
      t.attachment :image
    end
  end

  def self.down
    drop_attached_file :special_events, :image
  end
end
