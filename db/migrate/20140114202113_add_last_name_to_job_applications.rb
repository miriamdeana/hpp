class AddLastNameToJobApplications < ActiveRecord::Migration
  def change
    add_column :job_applications, :last_name, :string
  end
end
