require 'spec_helper'

describe ApplicationController do

  controller do
    def index
      render nothing: true
    end
  end

  describe "finding the current site based on hostname" do

    context "when found" do
      it "sets the current site as the tenant" do
        get :index
        ActsAsTenant.current_tenant.should == Site.first
      end
    end

    context "when not found" do

      before :each do
        request.host = "foobar.baz.com"
      end

      it "raises an exception" do
        expect { get :index }.to raise_error
      end
    end
  end
end