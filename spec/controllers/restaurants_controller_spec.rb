require 'spec_helper'

describe RestaurantsController do

  describe "GET 'mariahs'" do
    it "returns http success" do
      get 'mariahs'
      response.should be_success
    end
  end

  describe "GET '643'" do
    it "returns http success" do
      get '643'
      response.should be_success
    end
  end

  describe "GET 'tres_molinos'" do
    it "returns http success" do
      get 'tres_molinos'
      response.should be_success
    end
  end

  describe "GET 'pagoda'" do
    it "returns http success" do
      get 'pagoda'
      response.should be_success
    end
  end

  describe "GET 'brick_and_basil'" do
    it "returns http success" do
      get 'brick_and_basil'
      response.should be_success
    end
  end

end
