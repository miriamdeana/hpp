# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :job_opening do
    title "MyString"
    description "MyString"
    post_date "2013-12-30 11:23:23"
    closing_date "2013-12-30 11:23:23"
  end
end
