# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :menu_item, :class => 'MenuItems' do
    name "MyString"
    description "MyText"
    size "MyString"
    price "9.99"
    category "MyString"
    image ""
  end
end
