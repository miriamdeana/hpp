# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :special_event do
    date "2013-12-27"
    start_time "2013-12-27 15:30:42"
    end_time "2013-12-27 15:30:42"
    location "MyString"
    description "MyText"
  end
end
